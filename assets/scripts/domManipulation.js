const h1 = document.getElementById('main-title');


// Target the following Elements:

// -1 td- Benguet
h1.parentElement.nextElementSibling.lastElementChild.lastElementChild.lastElementChild

// 0. td- aklan

h1.parentElement.nextElementSibling.lastElementChild.lastElementChild.previousElementSibling.lastElementChild;

// 1. Body

h1.parentElement.parentElement.parentElement;

// 2. Thead
h1.parentElement.nextElementSibling.firstElementChild;

// 3. td - Palawan

h1.parentElement.nextElementSibling.lastElementChild.firstElementChild.lastElementChild;

// 4. th - spot

h1.parentElement.nextElementSibling.firstElementChild.firstElementChild.firstElementChild.nextElementSibling;
// 5. td - 2

h1.parentElement.nextElementSibling.lastElementChild.firstElementChild.nextElementSibling.firstElementChild;
// Submit here: wd005-1 DOM Manipulation 1: Selecting Elements

// Resume: 8PM Event Listeners



