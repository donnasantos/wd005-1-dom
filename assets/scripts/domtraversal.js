// // FIRST RULE:
// // CODE PROPERLY, INDENT PROPERLY
// Traversal from top --> down
// task target tbody via Id
// Solution: We Can use either? 
// document.getelementByID or
// document.querySelector

const tbody = document.getElementById('listBody');
// top getElementById
// document.querySelector('#listBody')

// to access first child.
// tbody.firstElementChild;

// to access last child
// tbody.lastElementChild

// to access parent
// tbody.parentElement

// traversal same-parent
// tbody.previousElementSibling

const thead = document.getElementById('listHead');
// thead.nextElementSibling
// tbody.firstElementChild

// Ex target --> frombody then target baguio

// body.firstElementChild.lastElementChild.lastElementChild.lastElementChild.firstElementChild.nextElementSibling




